﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ACIM365.Models;
using ACIM365.Services;

namespace ACIM365.Droid.Services
{
    public class LessonService : ILessonService
    {
        static List<Lesson> _lessons = new List<Lesson>();
        static List<string> _exceptIndexs = new List<string>
        {
            "N",
            "A:1",
            "A:51",
            "A:81",
            "A:111",
            "A:141",
            "A:171",
            "A:221",
            "A:231",
            "A:241",
            "A:251",
            "A:261",
            "A:271",
            "A:281",
            "A:291",
            "A:301",
            "A:311",
            "A:321",
            "A:331",
            "A:341",
            "A:351",
            "A:361"
        };

        public LessonService()
        {
            var stream = MainActivity.Instance.Assets.Open("acim_2.dat");
            var enuStream = MainActivity.Instance.Assets.Open("acim_enu_2.dat");

            if (_lessons.Count == 0)
                using (var lessonFile = new StreamReader(stream))
                {
                    using (var enuLessonFile = new StreamReader(enuStream))
                    {
                        var strLine = lessonFile.ReadLine();
                        var strEnuLine = enuLessonFile.ReadLine();
                        do
                        {
                            // 正则表达 匹配索引字符
                            var strIndex = Regex.Match(strLine, @"\【(.*)\】").Groups[1].Value;

                            // 排除不需要的课程目录
                            if (_exceptIndexs.IndexOf(strIndex) < 0)
                                _lessons.Add(new Lesson
                                {
                                    Index = strIndex,
                                    Content = strLine.Substring(strLine.IndexOf('】') + 1),
                                    EnuContent = strEnuLine.Substring(strLine.IndexOf('】') + 1),
                                });

                            strLine = lessonFile.ReadLine();
                            strEnuLine = enuLessonFile.ReadLine();
                        } while (strLine != null);
                    }
                }
        }

        public Lesson GetTitle(string index = "A:1:0")
        {
            return _lessons.Where(lesson => lesson.Index.Equals(index)).SingleOrDefault();
        }

        public IEnumerable<Lesson> GetContent(string index = "B:1:0")
        {
            return _lessons.Where(lesson => lesson.Index.Contains(index));
        }

        public Lesson GetNextTitle(string index = "A:1:0")
        {
            return _lessons.Where(lesson => lesson.Index.Contains("A"))
                    .SkipWhile(lesson => lesson.Index != index)
                    .Skip(1)
                    .FirstOrDefault();
        }

        public IEnumerable<Lesson> GetTitles()
        {
            return _lessons.Where(lesson => lesson.Index.Contains("A"));
        }
    }
}