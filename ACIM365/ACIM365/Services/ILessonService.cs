﻿using ACIM365.Models;
using System.Collections.Generic;

namespace ACIM365.Services
{
    public interface ILessonService
    {
        IEnumerable<Lesson> GetTitles();
        Lesson GetTitle(string index = "A:1:0");
        Lesson GetNextTitle(string index = "A:1:0");
        IEnumerable<Lesson> GetContent(string index = "B:1:0");
    }
}
