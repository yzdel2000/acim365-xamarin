﻿using ACIM365.Models;
using ACIM365.Services;
using ACIM365.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ACIM365.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CatalogView : ContentPage
    {
        private Lesson _curLesson;

        public CatalogView(Lesson curLesson)
        {
            _curLesson = curLesson;

            InitializeComponent();

            BindingContext = new CatalogViewModel();

            listTitle.SelectedItem = _curLesson;
            listTitle.ScrollTo(_curLesson, ScrollToPosition.Center, true);
        }

        private async void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selLesson = (e.Item as Lesson);
            if (selLesson != null)
            {
                (BindingContext as CatalogViewModel).ItemCommand.Execute(selLesson);
            }
            await Navigation.PopAsync();
        }
    }
}