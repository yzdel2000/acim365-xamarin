﻿namespace ACIM365.Models
{
    public class Lesson
    {
        public string Index { get; set; }
        public string Content { get; set; }
        public string EnuContent { get; set; }
    }
}
