﻿using ACIM365.Models;
using ACIM365.Services;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace ACIM365.ViewModels
{
    class CatalogViewModel : BaseViewModel
    {
        private ILessonService _lessonService;

        public CatalogViewModel()
        {
            _lessonService = DependencyService.Get<ILessonService>();

            Titles = new ObservableCollection<Lesson>(_lessonService.GetTitles());
        }

        ObservableCollection<Lesson> _titles;
        public ObservableCollection<Lesson> Titles
        {
            get
            {
                return _titles;
            }
            set
            {
                _titles = value;
                OnPropertyChanged("Titles");
            }
        }

        ICommand _itemCommand;
        public ICommand ItemCommand
        {
            get
            {
                _itemCommand = _itemCommand ?? new Command(execute: (e) =>
                {
                    MessagingCenter.Send<BaseViewModel, Lesson>(this, "SelectLesson", e as Lesson);
                });
                return _itemCommand;
            }
        }
    }
}
